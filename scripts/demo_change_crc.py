#!/usr/bin/env python3

# Overwrites map crc in demo file to reference different map
# Using numbers from:
# https://github.com/heinrich5991/libtw2/blob/master/doc/demo.md

import sys

# usage: demo_change_crc.py DEMO_FILE CRC

if len(sys.argv) != 3:
	print("usage: demo_change_crc.py DEMO_FILE MAP_CRC")
	print("example: demo_change_crc.py hammer.demo b48d6866")
	exit()

demo_file = sys.argv[1]
crc = sys.argv[2]

m = open(demo_file, "rb").read()
print("old crc:", m[0x8c:0x8c+4].hex())
print("new crc:", crc)
m = m[:0x8c] + bytes.fromhex(crc) + m[0x8c+4:]
f = open(demo_file, "wb")
f.write(m)
f.close()



