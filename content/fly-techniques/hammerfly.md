---
title: "Hammerfly"
weight: 10
---

### General Principle

Hammerfly is the most common flight technique for 2 tees.

To hammerfly:

- stack 2 tees on top of each other (use a wall to line up)
- the lower tee continuously hammers the upper tee
- the upper tee (the 'driver') hooks the lower tee

The hammer hits propell the driver upwards

The continuous hook:

- pulls the hammering tee upwards 
- keeps the two tees into hammer range

This way you can fly upwards indefinitely.

{{% vid hammerfly-wall %}}

#### Timing

Spamming the hammer and hook won't get you very high.

As the hammering tee, try to hammer whenever you just bounced off the other tee.

As the driver, never let go of the other tee in the air. Hooking every second hammer hit the lower tee does is a good rythm.

#### Without Lining Up

Whenever you don't have a wall to line up in a tower:

- choose a tee to drive
- have the driving tee jump over the other tee
- start the usual hammerfly cycle

{{% vid hammerfly-free %}}

### Driving

Driving allows the driving tee to direct the hammerfly to the side.
When done well, this allows for horizontal flight.

As the driver, drive by moving in the desired direction directly after you got hit by the hammer.

Start with very light movement. The further you move each hit, the more horizontal, but also unstable the hammerfly will get.

{{% vid hammerfly-drive %}}

#### Dropping While Hammerflying

Some parts require you to drop a bit during a hammerfly.

If the driver still has double jump available, you could:

- stop the hammerfly cycle shortly
- have the driver double jump to cancel the falling speed
- start the hammerfly cycle again

Without double jump, you can:

- stop the hammering
- have the driver hook the lower tee again and again to bump into each other

The bumping will slow down the drop significantly.
This way you can start the cycle any time you want.

Note however, that you want to have strong hook as the driver for this, else the bumping might not work as well.

### Kinta Fly

Kinta fly (or just 'kinta') is a special but much harder variant of hammerfly.
It is usually done below a ceiling and allows horizontal flight in much tighter space.

For kinta, both tees have to move in the desired direction at the same time.
Don't move continuously, but instead like you would move while driving.

What you want to achieve is that both tees stay above each other.
Note that you will have might have to adapt your hammering rhythm.

To initiate kinta successfully, start the moving at the same time.
Usually you start moving in the direction once the upper tee is near the ceiling.

### Speedfly

Speedfly can be attempted during a usual hammerfly to reach high vertical velocity.

Initiate speedfly as the driver by double jumping just before you will get hammered.
If you got the timing, you will suddenly gain a lot of height than usual on the hammer hit.

To keep the speed up, the hammering tee always has to hammer just before bumping into the upper tee, which would break it.

Pretty much every time window for this trick is very narrow.
