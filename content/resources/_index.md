---
title: "Resources"
date: 2019-11-09T21:48:15+01:00
weight: 100
---

All posts are sorted by creation date (or the best approximation for it)

## Services

* `2019-10-02`: [Teeskins](https://teeskins.de/) ([Github](https://github.com/iTzMeRafa/Teeskins)) by NeXus
* `2018-10-21`: [Teeworlds server status website](https://status.tw/) by MrCosmo
* `2015-07-09`: [DDNet Trashmap: Temporary map hosting](https://trashmap.timakro.de/) by timakro
* `2015-01-25`: [Skin Database](https://ddnet.tw/skins/) by DDNet
* `2013-08-28`: [Map collection](https://heinrich5991.de/teeworlds/maps/) by heinrich5991

## Blog Posts

* `2019-09-28`: [The Anatomy of a One Tick Unfreeze](https://heinrich5991.github.io/blog/blog/one-tick-unfreeze) by heinrich5991
* `2017-04-13`: [Preventing Cheats Due to Bugs](https://forum.ddnet.tw/viewtopic.php?f=9&t=5137)
* `2017-01-25`: [Movements: Mechanics of High Speed and Mastery](https://www.teeworlds.com/forum/viewtopic.php?id=12146) by Pathos
* `2016-01-10`: [DDNet Live: Twitch spectates an online game](https://hookrace.net/blog/ddnet-live/) by deen
* `2016-06-09`: [Experiences of Running an Online Game for 3 Years](https://hookrace.net/blog/ddnet-evolution-architecture-technology/) by deen
* `2016-05-14`: [DDNet Server Statistics with ServerStatus, RRDtool and Nim](https://hookrace.net/blog/server-statistics/) by deen
* `2015-09-28`: [DDNet keeps running](https://forum.ddnet.tw/viewtopic.php?t=2365) by Onion
* `2015-09-25`: [The End of DDraceNetwork](https://ddnet.tw/news/shutdown/) by deen
* `2015-07-16`: [The History of DDNet](//forum.ddnet.tw/viewtopic.php?f=3&t=1824) by deen
* `2015-07-14`: [DDRace/TW communities around the world](https://forum.ddnet.tw/viewtopic.php?f=6&t=1804) by deen
* `2015-07-09`: [DDNet Trashmap](https://forum.ddnet.tw/viewtopic.php?f=6&t=1764) by timakro
* `2011-09-15`: [Sound development is slower than speed of sound](https://teedune.wordpress.com/2011/09/15/sound-development-is-slower-than-speed-of-sound/) by Dune
* `2011-09-14`: [Teeworlds and file downloading](https://teedune.wordpress.com/2011/09/14/teeworlds-and-file-downloading/) by Dune
* `2011-09-12`: [Using custom sounds for Teeworlds](https://teedune.wordpress.com/2011/09/12/using-custom-sounds-for-teeworlds/) by Dune
* `2011-09-09`: [Teeworlds 0.7 - A pause command](https://teedune.wordpress.com/2011/09/09/a-pause-command/) by Dune
* `2011-09-08`: [Teeworlds 0.7 - An advanced scoreboard](https://teedune.wordpress.com/2011/09/08/an-advanced-scoreboard/) by Dune
* `2008-01-22`: [Tutorials: GAMEPLAY Movements](https://teeworlds.com/forum/viewtopic.php?id=449) by PrimeviL

## Communities

* English
  * `2014-08-09`[^ddnet-discord]: [DDNet Discord](https://ddnet.tw/discord)
  * `2014-05-05`[^ddnet-forum]: [DDNet Forum](http://forum.ddnet.tw/)
  * `2011-08-02`: [Teeworlds Reddit](https://old.reddit.com/r/teeworlds/)
  * Official Teeworlds IRC: #teeworlds on QuakeNet
  * https://qshar.com/
  * `2008-03-22`[^teeworlds-forum]: [Teeworlds Forum](https://teeworlds-friends.de/)

* German
  * `2015-05-05`: http://chillerdragon.weebly.com/
  * `2009-12-24`[^teeworlds-friends]: [Teeworlds Friends](https://teeworlds-friends.de/)

* Russian:
  * `2014-07-27`: http://teedes.ru/
  * `2011-08-17`: http://vk.com/twrus

* Poland:
  * https://www.facebook.com/teeworldspolska

## Documentation

Pages, which get extended over time.

* `2018-02-02`: [ArchWiki - DDRaceNetwork](https://wiki.archlinux.org/index.php/DDRaceNetwork) by RafaelFF
* `2015-09-02`: [Teeworlds technical documentation](https://github.com/heinrich5991/libtw2/tree/master/doc) by heinrich5991
* `2015-05-28`: [Game tiles explanations](https://ddnet.tw/explain/)
  by Lady Saavik (explanations) and Soreu, timakro ([code](https://github.com/ddnet/ddnet-web/tree/master/www/explain))
* `2014-05-05`: [DDNet Howto](https://ddnet.tw/oldhowto) by deen
* `2013-08-12`: [Teeworlds Documentation](https://github.com/teeworlds/documentation)
* `2013-08-10`: [Fandom Teeworlds Wiki about gametypes](https://teeworldsipod.fandom.com/wiki/Teeworlds_Wiki) by iPodClan
* `2011-06-16`: [Fandom Teeworlds Wiki about skins and development](https://teeworldsgame.fandom.com/wiki/Teeworlds:_Jumping_the_Gun_Wiki) by Jose Mendez
* `2010-12-16`: [The Teeworlds map format](https://sushitee.github.io/tml/mapformat.html) by erdbeere and SushiTee
* `2008-07-18`: [Fandom Teeworlds Wiki](https://teeworlds.fandom.com/wiki/Teeworlds_Wiki)

## Tutorials

* `2015-10-05`: [DDNet - Mapping ABC](https://forum.ddnet.tw/viewtopic.php?f=35&t=2419) by Index
* `2015-10-05`: [DDNet - Tutorials and other useful links](https://forum.ddnet.tw/viewtopic.php?f=16&t=2420) by Index

[^ddnet-discord]: https://forum.ddnet.tw/viewtopic.php?p=46942#p46942
[^ddnet-forum]: https://forum.ddnet.tw/viewtopic.php?f=3&t=2
[^teeworlds-forum]: https://www.teeworlds.com/?page=journal&id=942
[^teeworlds-friends]: https://teeworlds-friends.de/thread/9036-8-jahre-teeworlds-friends/
