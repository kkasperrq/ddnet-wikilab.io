---
title: "Creating videos"
weight: 20
---

## Settings

Set consistent skin:

* hammer: brownbear
* pistol: coala
* shotgun: cammo
* grenade: redstripe
* laser: bluestripe
* hook: default
* jump: bluekitty

Set zoom to 10: `cl_default_zoom 10`

## Maps

Set background to single color (currently `#5E84AE`).

Look at the existing videos to create your maps in a similar style.
Also make it loopable if possible.


## Demo to video

* https://github.com/ddnet/ddnet/pull/1928

Note: If the demo to video converter doesn't work for you, create a merge request with the demo and someone will take care of the video.

## Converting multiple videos in a loop

```
# convert all videos
for i in hammer-*.demo.mp4; do ./crop.sh $i $i.mp4; done
# check if all videos are 3 seconds long
for i in hammer-*.demo.mp4.mp4; do ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 $i; done
```

## Insert videos in pages

Adding {{\% vid vid-name %}} to markdown pages includes `/video/vid-name.mp4` in the html page.
